//Intergrated ACD
#include "arduinoFFT.h"
arduinoFFT FFT = arduinoFFT();
#include <Servo.h>
Servo myservo;

#define EN_PIN_E 8 // Nano v3: 16 Mega: 38 //enable (CFG6)
#define DIR_PIN_E 10 // 19 55 //direction
#define STEP_PIN_E 9 // 18 54 //step
#define EN_PIN_D 5 // Nano v3: 16 Mega: 38 //enable (CFG6)
#define DIR_PIN_D 7 // 19 55 //direction
#define STEP_PIN_D 6 // 18 54 //step
#define ServoPin 11
#define SAMPLES 128 //Must be a power of 2
#define SAMPLING_FREQUENCY 5000 //Hz, must be less than 10000 due to ADC

/* for simulation we use stepper motor, model 28BYJ-48, that has 64 steps per round. It includes a gear unit with
64:1 ratio, so
that for 1 rotation of the output shaft we need to generate 64*64=4096 steps */
const long f = 1000000; //frequency of the counter;
int Km = 6400, reduktor = 1, K = Km*reduktor,kasni=66;
// dspeed [rad/s]-desired speed, acce [rad/s^2]-acceleration
// dece[rad/s^2]-deceleration, ugao[deg]-angle
//float dspeed = 2, acce = 3, dece = 2, ugao = 0;
double alfa = TWO_PI/K;
long imaK = 0;
boolean smjer = true,stoj = true;
//uni-polar motor – we use microstepping manner of stepping
//byte cw[] = {0x01,0x03,0x02,0x06,0x04,0x0C,0x08,0x09};
//byte duzcw = sizeof(cw), kk = 0, maska = 0xF0; //
//char chh;
long A;
unsigned int sampling_period_us;
double vReal[SAMPLES];
double vImag[SAMPLES];
long LastS = 0;
int SPR = 6400;//steps per revolution
int pos = 162;//initial position of servo DO NOT CHANGE
float DisTvl = 0.0;
volatile bool BREAK = false;
unsigned int Mode = 1;
void setup() {
sampling_period_us = round(1000000*(1.0/SAMPLING_FREQUENCY));
DDRB |= B00000111;
DDRD |= B11100000;
pinMode(13,OUTPUT);
myservo.attach(ServoPin); // attaches the servo on pin 9 to the servo object

myservo.write(162);//back to closed position
pinMode(2,INPUT);
pinMode(3,INPUT);
digitalWrite(2, HIGH);
digitalWrite(3, HIGH);
//pinMode(STEP_PIN_E,OUTPUT);
//pinMode(DIR_PIN_E,OUTPUT);
//pinMode(EN_PIN_E,OUTPUT);
//pinMode(STEP_PIN_D,OUTPUT);
//pinMode(DIR_PIN_D,OUTPUT);
//pinMode(EN_PIN_D,OUTPUT);
Serial.begin(115200);//start communication
while(!Serial);
Serial.println("Start...");
delay(2000);
PORTB &= B11111000;
PORTD &= B00011111;
//digitalWrite(EN_PIN_E, LOW);
//digitalWrite(STEP_PIN_E, LOW);
//digitalWrite(DIR_PIN_E, LOW);
//digitalWrite(EN_PIN_D, LOW);
//digitalWrite(STEP_PIN_D, LOW);
//digitalWrite(DIR_PIN_D, LOW);
//myservo.write(162);//back to closed position
while(digitalRead(2) == HIGH);
delay(2000);
MoveStepperE(true,70000,1.5);
attachInterrupt(digitalPinToInterrupt(2), INT, FALLING);
attachInterrupt(digitalPinToInterrupt(3), EMS, FALLING);
}
void loop() {
///*
//testing
switch(Mode){
case(1):
//MoveStepperDWA(false,1600,1.0);
MoveStepperD(12.0,16.0,16.0,
-90.0);
if(FFToutput()){
digitalWrite(13,HIGH);
//MoveStepperD(true,6400,3.0);
OpenCage();
BREAK = false;
MoveStepperEINT(false,67000,3.0);
MoveStepperE(false,3000,2.0);
delay(500);
CloseCage();
MoveStepperE(true,LastS+3000 ,1.5);
LastS = 0;
MoveStepperD(6.0,12.0,12.0,
-720.0);
DisTvl = DisTvl
- 720.0;
Mode = 2;
}
else{
digitalWrite(13,LOW);
}
break;
case(2):
//MoveStepperDWA(false,1600,1.0);
MoveStepperD(12.0,16.0,16.0,
-90.0);

DisTvl = DisTvl
- 90.0;
if(FFToutput()){
digitalWrite(13,HIGH);
//MoveStepperD(true,6400,3.0);
BREAK = false;
MoveStepperEINT(false,67000,3.0);
MoveStepperE(false,3000,2.0);
OpenCage();
delay(500);
MoveStepperE(true,LastS+3000 ,1.5);
CloseCage();
LastS = 0;
Mode = 3;
}
else{
digitalWrite(13,LOW);
}
break;
case(3):
//MoveStepperDWA(true, DisTvl, 1.5);
MoveStepperD(12.0,16.0,16.0, (
-DisTvl));
OpenCage();
BREAK = false;
MoveStepperEINT(false,67000,3.0);
MoveStepperE(false,3000,2.0);
delay(500);
CloseCage();
MoveStepperE(true,LastS+3000 ,1.5);
LastS = 0;
Mode = 4;
break;
case(4):
//MoveStepperDWA(false, DisTvl, 1.5);
MoveStepperD(12.0,16.0,16.0, DisTvl);
BREAK = false;
MoveStepperEINT(false,67000,3.0);
MoveStepperE(false,3000,2.0);
OpenCage();
delay(500);
MoveStepperE(true,LastS+3000 ,1.5);
CloseCage();
LastS = 0;
Mode = 3;
break;
}
/*while(!FFToutput()){
MoveStepperD(true, 8000, 1.0);
}//move forward and search for module
OpenCage();
MoveStepperE(true, 8000, 1.0);//drop cage
CloseCage();
MoveStepperE(false, 8000, 1.0);//reel cage
MoveStepperD(true, 16000, 1.0);//moveout of the control module first
while(!FFToutput()){
    
MoveStepperD(true, 8000, 1.0);
}//move forward and search for module
MoveStepperE(true, 8000, 1.0);//drop cage
OpenCage();
MoveStepperE(false, 8000, 1.0);//reel cage
CloseCage();
MoveStepperD(false, 16000, 1.0);//moveout of the control module first, in opposite direction
while(!FFToutput()){
MoveStepperD(false, 8000, 1.0);
}//move backward and search for module
*/
}
void OpenCage(){
for (pos = 162; pos >= 100; pos -= 1) { // goes from 180 degrees to 0 degrees
myservo.write(pos); // tell servo to go to position in variable 'pos'
delay(15); // waits 15ms for the servo to reach the position
}
}
void CloseCage(){
for (pos = 100; pos <= 162; pos += 1) { // goes from 0 degrees to 180 degrees
// in steps of 1 degree
myservo.write(pos); // tell servo to go to position in variable 'pos'
delay(25); // waits 15ms for the servo to reach the position
}
}
void MoveStepperD( float dspeed, float acce, float dece, float ugao){
double Co, Cm, Ci,Ra,Rd;
long N = 0,nad = 0,na = 0, nd = 0, np = 0;
Ra=acce/(alfa*f*f); Rd=-dece/(alfa*f*f); Cm=alfa*f/dspeed;
N = (long) ugao*K/360.0 - imaK; stoj = false;
if (N == 0) {stoj = true; return; }
if (N > 0) smjer = true;
if (N < 0) {smjer = false; N = -N;}
if (N == 1) singleStepD(smjer);
// trajectory planning
if (acce != 0){// acceleration exists
nad = (long)(N*dece)/(acce+dece);
na = (long)(dspeed*dspeed)/(2*alfa*acce);
if (nad > na) { nd = N - na * acce/dece; }//case (i)
else { na = nad; nd = na; } //case (ii)
Co = f * sqrt(2*alfa/acce); Ci = Co;//Co is initial time delay
} else {//without acceleration
na = 0; nd = N; Ci = Cm;
}
if (nd < na) { // this is might be due to rounding
long np=na; na=nd;nd=np;//exchange the values of 'na' and 'nd'
} np = 0;
while(!stoj){
singleStepD(smjer);
delayMicroseconds(Ci - ((Ci == Cm) ? 4 : kasni) );

Ci = solveC(Co,Cm,Ci,Ra,Rd,N,nad,na,nd,np);
np=A;
if (++np >= N) { np = 0; kk = 0; Ci = Co; stoj = true;
Serial.print("Steps = ") ;Serial.print(imaK);
Serial.print(", angle = ");Serial.println(imaK*360/K);
}
}
}
void singleStepD(bool dir){
bool currentS = digitalRead(STEP_PIN_D);
if(dir){digitalWrite(DIR_PIN_D,HIGH);}
else{digitalWrite(DIR_PIN_D,LOW);}
if(currentS){PORTD &= B10111111;}
else{PORTD |= B01000000;}
}
/*void oneStep(boolean desno){
if (desno) {PORTB=(PORTB& maska) | cw[kk++]; imaK++;}
else {PORTB=(PORTB & maska) | cw[duzcw-1-kk++];imaK--;} if ( kk == duzcw ) kk = 0;
}
*/
//solve the time delay
double solveC(double Co, double Cm, double Ci, double Ra, double Rd, long N, long nad, long na, long nd,long
np){
if (na == 0) return (Cm);
if ( (np<=na)||(np>=nd) ){//acceleration and deceleration phase
double q = 1+( (np<=na)?Ra:Rd )*Ci*Ci; Ci = Ci/q;kasni=66;
if ( ( (np>= 1) && (np<= 5)) || ((N-np <= 5) && ( N-np >= 1))){ Ci = Ci*(1+0.08/np) ; kasni=120;}//correction
if (Ci < Cm) Ci = Cm; //using variable ‘kasni’ we take into
}
else Ci = Cm;
A = np;
return Ci; //account all delays occurred during the call and execute of program
}
/*
void MoveStepperD(bool dir, unsigned long Steps,float Angular_velocity){
bool currentS = digitalRead(STEP_PIN_D);
unsigned long Ctime = 0;
unsigned long nextTime;
unsigned long steps = 0;
unsigned long lastTime;
lastTime = micros();
nextTime = lastTime+round(1000000/SPR/Angular_velocity);
if(dir){
digitalWrite(DIR_PIN_D,HIGH);
}
else{
digitalWrite(DIR_PIN_D,LOW);
}
while(steps<=Steps){
Ctime = micros();
if(currentS){
    
if(nextTime<=Ctime){
PORTD &= B10111111;
//digitalWrite(STEP_PIN_D, LOW);
nextTime = Ctime+round(1000000/SPR/Angular_velocity);
currentS = !currentS;
steps++;
}
}
else{
if(nextTime<=Ctime){
PORTD |= B01000000;
//digitalWrite(STEP_PIN_D, HIGH);
nextTime = Ctime+round(1000000/SPR/Angular_velocity);
currentS = !currentS;
steps++;
}
}
//
if ((Ctime
- lastTime) >= 1000000) {
if (dir) {
PORTB |= B10000000;
//digitalWrite(DIR_PIN_D,HIGH);
} else {
PORTB &= B01111111;
//digitalWrite(DIR_PIN_D,LOW);
}
dir = !dir;
lastTime = Ctime;
}
//}
}
*/
void MoveStepperDWA(bool dir, unsigned long Steps,float Angular_velocity){
bool currentS = digitalRead(STEP_PIN_D);
unsigned long Ctime = 0;
unsigned long nextTime;
double steps = 0.0;
unsigned long lastTime;
lastTime = micros();
nextTime = lastTime+round(1562.5);
if(dir){
digitalWrite(DIR_PIN_D,HIGH);
}
else{
digitalWrite(DIR_PIN_D,LOW);
}
while(steps<=800.0){
Ctime = micros();
if(currentS){
if(nextTime<=Ctime){
PORTD &= B10111111;
//digitalWrite(STEP_PIN_D, LOW);
nextTime = Ctime+round(((1562.5
-(1000000/SPR/Angular_velocity))*(1
-
(steps/800)))+(1000000/SPR/Angular_velocity));
currentS = !currentS;
steps++;
}
}
else{
    
if(nextTime<=Ctime){
PORTD |= B01000000;
//digitalWrite(STEP_PIN_D, HIGH);
nextTime = Ctime+round(((1562.5
-(1000000/SPR/Angular_velocity))*(1
-
(steps/800)))+(1000000/SPR/Angular_velocity));
currentS = !currentS;
steps++;
}
}
/*
if ((Ctime
- lastTime) >= 1000000) {
if (dir) {
PORTB |= B10000000;
//digitalWrite(DIR_PIN_D,HIGH);
} else {
PORTB &= B01111111;
//digitalWrite(DIR_PIN_D,LOW);
}
dir = !dir;
lastTime = Ctime;
}
*/}
while(steps>=800.0 & steps<= Steps
-800.0){
Ctime = micros();
if(currentS){
if(nextTime<=Ctime){
PORTD &= B10111111;
//digitalWrite(STEP_PIN_D, LOW);
nextTime = Ctime+round(1000000/SPR/Angular_velocity);
currentS = !currentS;
steps++;
}
}
else{
if(nextTime<=Ctime){
PORTD |= B01000000;
//digitalWrite(STEP_PIN_D, HIGH);
nextTime = Ctime+round(1000000/SPR/Angular_velocity);
currentS = !currentS;
steps++;
}
}
/*
if ((Ctime
- lastTime) >= 1000000) {
if (dir) {
PORTB |= B10000000;
//digitalWrite(DIR_PIN_D,HIGH);
} else {
PORTB &= B01111111;
//digitalWrite(DIR_PIN_D,LOW);
}
dir = !dir;
lastTime = Ctime;
}
*/}
while(steps<=Steps){
Ctime = micros();
if(currentS){
    
if(nextTime<=Ctime){
PORTD &= B10111111;
//digitalWrite(STEP_PIN_D, LOW);
nextTime = Ctime+round(((1562.5
-(1000000/SPR/Angular_velocity))*(1
-((Steps
-
steps)/800)))+(1000000/SPR/Angular_velocity));
currentS = !currentS;
steps++;
}
}
else{
if(nextTime<=Ctime){
PORTD |= B01000000;
//digitalWrite(STEP_PIN_D, HIGH);
nextTime = Ctime+round(((1562.5
-(1000000/SPR/Angular_velocity))*(1
-((Steps
-
steps)/800)))+(1000000/SPR/Angular_velocity));
currentS = !currentS;
steps++;
}
}
/*
if ((Ctime
- lastTime) >= 1000000) {
if (dir) {
PORTB |= B10000000;
//digitalWrite(DIR_PIN_D,HIGH);
} else {
PORTB &= B01111111;
//digitalWrite(DIR_PIN_D,LOW);
}
dir = !dir;
lastTime = Ctime;
}
*/}
}
void MoveStepperE(bool dir, unsigned long Steps,float Angular_velocity)
{
bool currentS = digitalRead(STEP_PIN_E);
unsigned long Ctime = 0;
unsigned long nextTime;
unsigned long steps = 0;
unsigned long lastTime;
lastTime = micros();
nextTime = lastTime+long(1000000/SPR/Angular_velocity);
if(dir){
digitalWrite(DIR_PIN_E,HIGH);
}
else{
digitalWrite(DIR_PIN_E,LOW);
}
while(steps<=Steps){
Ctime = micros();
if(currentS){
if(nextTime<=Ctime){
PORTB &= B11111101;
//digitalWrite(STEP_PIN_E, LOW);
nextTime = Ctime+long(1000000/SPR/Angular_velocity);
currentS = !currentS;
steps++;
}
}
else{
    
if(nextTime<=Ctime){
PORTB |= B00000010;
//digitalWrite(STEP_PIN_E, HIGH);
nextTime = Ctime+long(1000000/SPR/Angular_velocity);
currentS = !currentS;
steps++;
}
}
/*
if ((Ctime
- lastTime) >= 1000000) {
if (dir) {
PORTB |= B00000100;
//digitalWrite(DIR_PIN_E,HIGH);
} else {
PORTB &= B11111011;
//digitalWrite(DIR_PIN_E,LOW);
}
dir = !dir;
lastTime = Ctime;
}
*/}
}
void MoveStepperEINT(bool dir, unsigned long Steps,float Angular_velocity){
bool currentS = digitalRead(STEP_PIN_E);
unsigned long Ctime = 0;
unsigned long nextTime;
unsigned long steps = 0;
unsigned long lastTime;
lastTime = micros();
nextTime = lastTime+long(1000000/SPR/Angular_velocity);
if(dir){
digitalWrite(DIR_PIN_E,HIGH);
}
else{
digitalWrite(DIR_PIN_E,LOW);
}
while(steps<=Steps){
Ctime = micros();
if(BREAK){
BREAK = false;
LastS = steps;
break;
}
if(currentS){
if(nextTime<=Ctime){
PORTB &= B11111101;
//digitalWrite(STEP_PIN_E, LOW);
nextTime = Ctime+long(1000000/SPR/Angular_velocity);
currentS = !currentS;
steps++;
}
}
else{
if(nextTime<=Ctime){
PORTB |= B00000010;
//digitalWrite(STEP_PIN_E, HIGH);
nextTime = Ctime+long(1000000/SPR/Angular_velocity);
currentS = !currentS;

steps++;
}
}
/*
if ((Ctime - lastTime) >= 1000000) {
if (dir) {
PORTB |= B00000100;
//digitalWrite(DIR_PIN_E,HIGH);
} else {
PORTB &= B11111011;
//digitalWrite(DIR_PIN_E,LOW);
}
dir = !dir;
lastTime = Ctime;
}
*/
LastS = steps;
}
}
bool FFToutput(){
bool output;
unsigned long microseconds;
double TFA = 0.0;
double OFA = 0.0;
for(int i=0; i<SAMPLES; i++)
{
microseconds = micros(); //Overflows after around 70 minutes!
vReal[i] = analogRead(0)-345;
vImag[i] = 0;
while(micros() < (microseconds + sampling_period_us)){
}
}
/*FFT*/
FFT.Windowing(vReal, SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);
FFT.Compute(vReal, vImag, SAMPLES, FFT_FORWARD);
FFT.ComplexToMagnitude(vReal, vImag, SAMPLES);
double peak = FFT.MajorPeak(vReal, SAMPLES, SAMPLING_FREQUENCY);
for(int i=48; i<=52; i++){
TFA = TFA+vReal[i];
}
for(int i=40; i<=60; i++){
OFA = OFA+vReal[i];
}
OFA = OFA-TFA;
Serial.println(TFA-OFA);
if(TFA > OFA){
output = true;
}
else{
output = false;
}
return(output);
}
void INT(){
BREAK = true;

}
void EMS(){
while(true);
}


// Independently written by Steven Zhang