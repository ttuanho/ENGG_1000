# ENGG_1000 : Engineering Design and Innovation
All materials provided in project ACD in ENGG1000 @ UNSW (2019T1)

## Project: Autonomous Container Delivery (ACD)

### Introduction
Developing methods to automatically move objects and materials has fascinated inventors and engineers throughout history. Since the birth of science fiction, automatic, self-navigating equipment and vehicles have been a key recurring theme. Today, autonomous delivery of large goods is technically feasible and becoming a reality in [mines](https://www.youtube.com/watch?v=m4wLPfOz-c4), [ports](https://www.youtube.com/watch?v=6t8NcD4bWDk) and [warehouses](https://www.youtube.com/watch?v=cLVCGEmkJs0). In this project, we will build our own autonomous vehicles, learn about some of their basic electric principles, and hopefully have plenty of fun on the journey. (see the [design brief](https://github.com/ttuanho/ENGG_1000/blob/master/pdfs/Project%20Intro/ENGG1000_Project%20Brief_T1%202019.pdf))

### Problem Definition
The problem to be solved is that of autonomously moving fragile cargo from an initially unknown pickup location position to an initially unknown delivery location, as quickly and accurately as possible without damaging the cargo. The pickup and delivery locations will be wirelessly communicated to the delivery system by control modules after the system has started moving. (see the [design brief](https://github.com/ttuanho/ENGG_1000/blob/master/pdfs/Project%20Intro/ENGG1000_Project%20Brief_T1%202019.pdf))

### EVALUATION OF FINAL SOLUTIONS
Multiple criteria will be employed in scoring the final systems, in priority order of weight:
#### Performance:
Performance will be measured in terms of:
* The speed of the delivery. This will be the equal most heavily weighted criterion.
* The accuracy of the delivery2. This will be the equal most heavily weighted criterion.
* Whether the cargo is delivered safely (no breakages!).
* How robustly the system can detect the control module.
Bonus marks will be awarded for correctly picking up and delivering more than one egg in a single delivery (note: eggs will be collected and delivered from/to the same points each time) if requested by the control module (more detail below).
During final testing, two attempts may be made. Part marks will be awarded for all correct functions if the delivery is not successful.
#### Robustness
This will be subjectively determined based on criteria such as: whether correct delivery can be demonstrated multiple times, how the system would function if someone shook it slightly or dropped or bumped it and how much background noise the system’s sensor can tolerate.
#### Innovation
This will be subjectively determined by the panel of judges based on the uniqueness of your design in comparison to the other entries. In general, this is related to the way in which you utilise technology to perform the primary functions
of the design and the degree of difference between your solution and the other competitors, but is also related to the simplicity and elegance of your solution (simpler systems are cheaper to manufacture).
#### Aesthetic Appeal
This will be subjectively determined by the panel of judges based on the visual attractiveness, use of a theme or visual novelty.
(see the [design brief](https://github.com/ttuanho/ENGG_1000/blob/master/pdfs/Project%20Intro/ENGG1000_Project%20Brief_T1%202019.pdf))